#!/usr/bin/env python3

import subprocess
from time import sleep

# to-do: ESC to cancel

def notify(time):
    msgid = 55701
    icon = "/usr/share/icons/gnome/scalable/status/channel-secure-symbolic.svg"
    command = f"dunstify -a 'Lock' -u normal -i {icon} -r {msgid} 'Will lock in: {time}..' 'Press ESC to exit'"
    subprocess.run(command, shell=True)


def main():
    # Show notification
    countdown = 3
    for i in range(countdown):
        notify(countdown-i)
        sleep(1)

    # Close notification
    subprocess.run("dunstify -C 55701", shell=True)

    # Lockscreen not implemented yet
    # lock()

    # Suspend
    subprocess.run("systemctl suspend",shell=True)

if __name__ == "__main__":
    main()
