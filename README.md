<img src="assets/banner.png" align=center height=300px>

## 📖 Description
**<a href="http://www.qtile.org/">Qtile</a>** is a dynamic window manager for x11 written entirely in python.<br>
Is full-featured, highly customizable and extensive by the same programming language.<br>
It has features such as multiple layouts for windows, multi-monitors suppport and <br>
its own status bar with multiple widgtes.<br>

🪶 This repository only contains my config for Qtile, <br>
which is running on the `0.17.0` version.

<br>

##### 🚀 *QUICK INSTALL*

Choose one method: <br>
```sh
# + Using Curl
$ sh -c "$(curl -fsSL https://gitlab.com/yowls/qtwm/-/raw/main/scripts/misc/setup.sh)"

# + Using Wget
$ sh -c "$(wget -O- https://gitlab.com/yowls/qtwm/-/raw/main/scripts/misc/setup.sh)"

# + Using Fetch
$ sh -c "$(fetch -o - https://gitlab.com/yowls/qtwm/-/raw/main/scripts/misc/setup.sh)"
```

*=> [ [source script](scripts/misc/setup.sh) ]*

<br>
<br>

## ✨ Features
+ Change easily between multiples _Status-Bar themes_
+ Change easily between multiples global _Color-Schemes_
+ Synchronize color of applications with choosen color scheme
+ Status bar with multi monitor support
+ Use the built-in bar _as a dock_
+ _Dropdown_ your terminal of choice
+ Rules for floating windows and window matching
+ Set all your _preferences in one file_
+ Easily extensible with python

<br>
<br>

## 🖼️ Previews
<img src="https://i.imgur.com/Eh6ke3c.png" alt="Minla - Nihilist"> <br>

`Details`
+ **Color Scheme**: [Nihilist](themes/color_schemes/nihilist.py)
+ **Bar theme**: [Minla](themes/status_bar/Minla)
+ **Terminal**: Kitty
+ **Wallpaper**: [source](https://www.reddit.com/r/MechanicalKeyboards/comments/m8sgpv/bumper_60_keyboard_an_elegant_case_with_brass/?utm_source=share&utm_medium=web2x&context=3)

<br>
<br>

## 📜 Documentation
##### > [ [Home Wiki](https://gitlab.com/yowls/qtwm/-/wikis/home) ]
##### > [ [Installation](https://gitlab.com/yowls/qtwm/-/wikis/Install) ]
##### > [ [Explanation of the code](https://gitlab.com/yowls/qtwm/-/wikis/explanation-of-code) ]
