from libqtile.config		import EzKey as Key
from libqtile.command		import lazy
from library.helpers.keybinds	import *

##########
# SYSTEM #
##########

def system_keys():
	return [
		# => Volume
		Key("<XF86AudioRaiseVolume>",
			lazy.function(launch_script, "system/volume", "up"),
			desc = "Volume up"
		),
		Key("<XF86AudioLowerVolume>",
			lazy.function(launch_script, "system/volume", "down"),
			desc = "Volume down"
		),
		Key("<XF86AudioMute>",
			lazy.spawn("volume toggle-audio"),
			desc = "Mute volume"
		),

		# => Microphone
		Key("<XF86AudioMicMute>",
			lazy.spawn("volume toggle-microphone"),
			desc = "Toggle Mute Microphone"
		),

		# => Music
		Key("<XF86AudioNext>",
			lazy.spawn("playerctl next"),
			desc = "Play next"
		),
		Key("<XF86AudioPrev>",
			lazy.spawn("playerctl previous"),
			desc = "Play previous"
		),
		Key("<XF86AudioPlay>",
			lazy.spawn("playerctl play-pause"),
			desc = "Toggle Play"
		),

		# => Brightness
		Key("<XF86MonBrightnessUp>",
			lazy.function(launch_script, "system/brightness", "up"),
			desc = "Brightness Up"
		),
		Key("<XF86MonBrightnessDown>",
			lazy.function(launch_script, "system/brightness", "down"),
			desc = "Brightness Down"
		),

		# => Session management
		Key("M-C-p",
			lazy.spawn("rofi -show powermenu -modi powermenu:rofi-power-menu"),
			desc = "Session menu"
		),
		#  Key("M-C-l",
		#          lazy.function(launch, "scripts/lock_now.py"),
		#          desc = "Lock the session and then suspend"
		#  ),
		#  Key("XF86PowerOff",
		#          lazy.spawn("systemctl poweroff"),
		#          desc = "Ask for shutdown"
		#  ),
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
