from libqtile.config	import EzKey as Key
from libqtile.command	import lazy

#########
# MOVE #
#########

def move_keys():
	return [
		#  Key("M-C-a",
		#          lazy.function(floating_center),
		#          desc = "Move floating window"
		#  ),

		Key("M-C-k",
			lazy.layout.shuffle_up(),	# general
			lazy.layout.section_up(),	# tree tab
			desc = "Move window up in the stack"
		),

		Key("M-C-j",
			lazy.layout.shuffle_down(),	# general
			lazy.layout.section_down(),	# tree tab
			desc = "Move window down in the stack"
		),

		Key("M-C-h",
			lazy.layout.shuffle_left(),	# general
			lazy.layout.swap_left(),	# monad
			desc = "Move window to the left"
		),

		Key("M-C-l",
			lazy.layout.shuffle_right(),	# general
			lazy.layout.swap_right(),	# monad
			desc = "Move window to the right"
		)
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
