import subprocess
import time
from libqtile.log_utils import logger


#
## SEND NOTIFICATION
#

# Send a simple notification
# TODO: set default value for title, msg
def notify (title, msg):
    icon = "/usr/share/icons/gnome/scalable/status/dialog-warning-symbolic.svg"
    command = f"dunstify -a 'errorConfig' -u normal -i {icon} '{title}' '{msg}'"
    subprocess.Popen(command, shell=True, stdout=subprocess.DEVNULL)
    return


# Send a critical notification
def notify_error(err):
    icon = "/usr/share/icons/gnome/scalable/status/dialog-warning-symbolic.svg"
    command = f"dunstify -a 'errorConfig' -u critical -i {icon} 'Error in config' '{err}'"
    subprocess.Popen(command, shell=True, stdout=subprocess.DEVNULL)
    return


#
## WRITE LOG TO A FILE
#

# Calculate time in ms
def timing():
    return int(round(time.time() * 1000))


def log(*args):
    logger.warning("----------------------------")
    logger.warning(*args)
    logger.warning("----------------------------")
    return

