import subprocess
from libqtile import hook
from libqtile.log_utils import logger
from settings.paths import qtile_dir

@hook.subscribe.startup_once
def load_once():
    from library.autostart.hooks import start_hooks
    start_hooks()

    # os.environ["QTILE_DIR"] = os.path.expanduser("~/.config/qtile/")
    # os.environ["QT_QPA_PLATFORMTHEME"] = "qt5ct"

    return


@hook.subscribe.startup_complete
def load_complete():
    # Set the wallpaper based on settings/wallpaper
    autostart_wallpaper_path = qtile_dir + "library/autostart/wallpaper.py"
    subprocess.Popen(autostart_wallpaper_path, shell=True)

    # Run applications from settings/autostart_apps
    autostart_apps_path = qtile_dir + "library/autostart/apps.py"
    subprocess.Popen(autostart_apps_path, shell=True)

    # TODO: move log timing here

    return


# -------------
# Import this function to run hooks as side effect
def init():
    logger.warning("-> Autostart scripts loaded")
