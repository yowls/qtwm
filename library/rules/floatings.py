from libqtile        import layout
from libqtile.config import Match

# Run the utility of `xprop` to see the details of an X client.
# options: <title>, <wm_class>, <role>, <wm_type>, <wm_instance_class>, <net_wm_pid>

# Set application in floating layout
def floating_rules(layout_theme):
	# Increase border for floating windows
	layout_theme['border_width'] +=3

	return layout.Floating(
		float_rules = [
			*layout.Floating.default_float_rules,
			Match(wm_class = 'ssh-askpass'),	# ssh-askpass
			Match(wm_class = 'confirmreset'),	# gitk
			Match(wm_class = 'makebranch'),		# gitk
			Match(wm_class = 'maketag'),		# gitk
			Match(title    = 'branchdialog'),	# gitk
			Match(title    = 'pinentry'),		# GPG key password entry

			# Firefox places instance
			Match(title = 'Biblioteca'),
			Match(title = 'Library'),
			Match(title = 'Acerca de Mozilla Firefox'),
			Match(title = 'About Mozilla Firefox'),

			# Applications
			Match(wm_class = 'Arandr'),
			Match(wm_class = 'Xephyr'),
			Match(wm_class = 'URxvt'),
			Match(wm_class = 'XTerm'),
			Match(wm_class = 'Pcmanfm'),
			Match(wm_class = 'Synaptic'),
			Match(wm_class = 'Gparted'),
			Match(wm_class = 'Pavucontrol'),		# pulse audio
			Match(wm_class = 'kfontinst'),
			Match(wm_class = 'MEGAsync'),
			Match(wm_class = 'pcloud'),
			Match(wm_class = 'zoom'),
			Match(wm_class = 'vlc'),
			Match(wm_class = 'feh'),
			Match(wm_class = 'ark'),
			Match(wm_class = 'kcalc'),
			Match(wm_class = 'kwalletmanager5'),
			Match(wm_class = 'Lxappearance'),
			Match(wm_class = 'qt5ct'),
			Match(wm_class = 'Kvantum Manager'),
			Match(wm_class = 'Tk'),
			Match(wm_class = 'Xscreensaver-demo')
		],
		**layout_theme
	)
# vim: tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab nowrap
