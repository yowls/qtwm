#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#1c2541",
    'bg-grad':  ["#1c2541", "#1c2541"],

    'fg':           "#f4f1de",
    'fg-inactive':  "#c4c4c8",

    # Special
    'accent':   "#00a896",
    'alert':    "#f3722c",

    # Scheme
    'color1':   "#f3722c",
    'color2':   "#00a896",
    'color3':   "#f0f3bd",
    'color4':   "#028090",
    'color5':   "#05668d",
    'color6':   "#02c39a"
}
