#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#333641",
    'bg-grad':  ["#333641", "#494e5e"],

    'fg':           "#f5f6fa",
    'fg-inactive':  "#a2a2a4",

    # Special
    'accent':   "#778beb",
    'alert':    "#ff4d4d",

    # Palette
    'color1':   "#ff4d4d",
    'color2':   "#38ada9",
    'color3':   "#f7d794",
    'color4':   "#546de5",
    'color5':   "#786fa6",
    'color6':   "#778beb"
}
