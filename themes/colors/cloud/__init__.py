#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#dee2ff",
    'bg-grad':  ["#dee2ff", "#b6ccfe"],

    'fg':           "#333641",
    'fg-inactive':  "#415a77",

    # Special
    'accent':   "#8e9aaf",
    'alert':    "#bc4b51",

    # Scheme
    'color1':   "#0091ad",
    'color2':   "#1b263b",
    'color3':   "#33415c",
    'color4':   "#5c677d",
    'color5':   "#284b63",
    'color6':   "#242423"
}
