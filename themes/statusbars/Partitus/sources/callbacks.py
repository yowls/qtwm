#    MOUSE CALLBACKS
#---------------------------
# functions executed when touching a widget in bar

def run(qtile, arg):
    qtile.cmd_spawn(arg)

def toggle_bar(qtile):
    qtile.cmd_hide_show_bar(position="left")

def launch_htop(qtile):
    qtile.cmd_spawn("kitty -e htop")
