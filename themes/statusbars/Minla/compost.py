##############
# STATUS BAR #
##############
# create a simple and compacted status bar
# with diferent widgets up and down

from libqtile import bar
from .sources import style, widgets


def init():
    return bar.Bar(
        [
            # >> LEFT SIDE
            widgets.separation_medium,

            widgets.window_tab,

            widgets.spacer,

            # >> RIGHT SIDE
            widgets.wallpaper_icon,
            widgets.wallpaper_text,

            widgets.separation_high,

            widgets.bitcoin_icon,
            widgets.bitcoin_text,

            widgets.separation_high,

            widgets.diskfree_icon,
            widgets.diskfree_text,

            widgets.separation_high,

            widgets.network_icon,
            widgets.network_text,

            widgets.separation_high,

            widgets.thermal_sensor_icon,
            widgets.thermal_sensor_text,

            widgets.separation_high,

            widgets.exit_icon,
            widgets.exit_text,

            widgets.separation_medium,
        ],
        style.height,
        background=style.bg_color,
        margin=style.margin,
        opacity=1,
    )
