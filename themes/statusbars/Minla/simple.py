##############
# STATUS BAR #
##############
# create a simple and compacted status bar

from libqtile import bar
from .sources import style, widgets


def init():
    return bar.Bar(
        [
            # >> LEFT SIDE
            widgets.separation_medium,

            widgets.memory_icon,
            widgets.memory_text,

            widgets.separation_high,

            widgets.groups,

            widgets.separation_high,

            widgets.prompt,


            # >> CENTER SIDE
            # FIXME: two spacer here break the status bar
            widgets.window_name,


            # >> RIGHT SIDE
            widgets.system_tray,

            widgets.separation_high,

            widgets.battery_icon,
            widgets.battery_text,

            widgets.separation_high,

            widgets.date_icon,
            widgets.date_text,

            widgets.separation_high,

            widgets.clock_text,
            widgets.clock_icon,

            widgets.separation_high,

            widgets.power_menu,

            widgets.separation_medium,
        ],
        style.height,
        background=style.bg_color,
        margin=style.margin,
        opacity=1,
    )
