# Load qtile libs for set the bar
from libqtile.config import Screen
from libqtile import bar, widget

# Load my custom config
from themes.statusbar.Simpleone.one_line import init_bar as init_main_bar
from themes.statusbar.Simpleone.sources.style import *
# from themes.statusbar.Simpleone.sources.callbacks import ..


#    DEFINE STATUS BAR
#---------------------------
# create a simple and compacted status bar
# with diferent widgets up and down

def init_extensive_bar(colors):
    qtile_bar = bar.Bar([
        # Window Tabs
            widget.WindowTabs(
                background = colors['bg'],
                foreground = colors['fg'],
                selected = ('<i>', '</i>'),
                separator = ' 🍈 ',
                max_chars = 30,
                **widget_defaults
            ),
            widget.Spacer(background  = colors['bg']),

        #--- RIGHT SIDE
        # BITCOIN
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.BitcoinTicker(
                background = colors['accent'],
                foreground = colors['fg'],
                currency    = 'USD',
                update_interval = 600,
                url = None,
                **widget_defaults
            ),
        #---
        # DISK FREE
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.DF(
                background = colors['accent'],
                foreground = colors['fg'],
                warn_color  = colors['alert'],
                format      = '{uf}{m}|{r:.0f}%',
                partition   = '/',
                measure     = 'G',
                warn_space  = 2,
                visible_on_warn = False,
                update_interval = 600,
                **widget_defaults
            ),
        #---
        # NOTIFY
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.Net(
                background = colors['accent'],
                foreground = colors['fg'],
                format = '{down}↓ ↑{up}',
                update_interval = 3,
                use_bits = False,
                **widget_defaults
            ),
        #---
        # THERMAL SENSORS
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.ThermalSensor(
                background = colors['accent'],
                foreground = colors['fg'],
                metric      = True,
                show_tag    = False,
                threshold   = 70,
                update_interval = 10,
                **widget_defaults
            ),
        #---
        # QUICK EXIT
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.QuickExit(
                background = colors['accent'],
                foreground = colors['fg'],
                countdown_format = '{} seconds',
                countdown_start = 5,
                timer_interval = 1,
                default_text = ' Exit',
                **widget_defaults
            ),
        #---
            widget.Sep(
                background= colors['accent'],
                padding = 15,
                linewidth = 0
            )
        ], bar_height, margin=bar_margin, background=colors['bg'], opacity=0.9)
    return qtile_bar


# Main bar is defined in the one-line file
# so i just create another bar and append to the original
def split_screens(_dock, color_scheme, _monitors):
    screens = []
    fake_screens = []

    for i in range(_monitors):
        main_widgets = init_main_bar(color_scheme)

        # Widgets in main monitor
        if i == 0:
            extensive_widgets = init_extensive_bar(color_scheme)
            if _dock == "none":
                main_screen = Screen(bottom=main_widgets, top=extensive_widgets)
                screens.append(main_screen)

            else:
                setup_dock = __import__('Bars.docks.'+_dock, fromlist=['init_dock']).init_dock
                dock_widgets = setup_dock(color_scheme)

                if _dock == "vertical":
                    main_screen_dockerized = Screen(bottom=main_widgets, top=extensive_widgets, left=dock_widgets)
                elif _dock == "horizontal":
                    # Incompatible
                    main_screen_dockerized = Screen(bottom=main_widgets, top=extensive_widgets)
                screens.append(main_screen_dockerized)

        else:
            # Replicate one_line widgets for other monitors
            other_monitor_with_widgets = Screen(bottom=main_widgets)
            screens.append(other_monitor_with_widgets)

    return screens, fake_screens
