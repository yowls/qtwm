from themes.colors.current import colors

############
# SETTINGS #
############
# Define general style settings

# => Measurements (in pixels)
dock_height = 40
dock_margin = 5

# => Font
font_text = "InconsolataNerdFontMono"
icon_size = 38

# => Colors
bg_color = colors["bg"]  # Dock background


##############
# SET CONFIG #
##############

default_separation = {"padding": 20, "linewidth": 0}

textbox_defaults = {"font": font_text, "fontsize": icon_size}
