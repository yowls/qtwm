###################
# HORIZONTAL DOCK #
###################
# set a centered horizontal bar with applications

from libqtile import bar
from .sources import style, widgets


def init_dock():
    return bar.Bar(
        [
            widgets.spacer,

            widgets.hide_bar,
            widgets.separation_high,

            widgets.terminal,
            widgets.separation_medium,

            widgets.browser,
            widgets.separation_medium,

            widgets.file_manager,
            widgets.separation_medium,

            widgets.text_editor,
            widgets.separation_medium,

            widgets.telegram,

            widgets.spacer
        ],
        style.dock_height,
        margin=style.dock_margin,
        background=style.bg_color
    )
