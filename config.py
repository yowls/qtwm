#  ______     ______    __     __         ______
# /\  __ \   /\__  _\  /\ \   /\ \       /\  ___\
# \ \ \/\_\  \/_/\ \/  \ \ \  \ \ \____  \ \  __\
#  \ \___\_\    \ \_\   \ \_\  \ \_____\  \ \_____\
#   \/___/_/     \/_/    \/_/   \/_____/   \/_____/
#     gitlab.com/yowls/qtilewm-rice

##################
# IMPORT MODULES #
##################

# Python libraries

# Qtile libraries

# Import custom config
from library import groups as lgroups, layouts as llayouts
from library import rules, keybinds, autostart
from library.helpers import debug
import themes


#############
# PRE-START #
#############

# Set initial mark for boot time
start = debug.timing()


###########
# THEMING #
###########

cs = themes.init_color_scheme()

widget_defaults = themes.init_style_default()
extension_defaults = widget_defaults

screens, fake_screens = themes.init_statusbar()


###########
# LAYOUTS #
###########

layout_theme = llayouts.init_layout_theme(cs)
layouts = llayouts.init_layout(layout_theme, cs)


#########
# RULES #
#########

group_rules = rules.groups_match()
floating_layout = rules.floating_rules(layout_theme)
dgroups_app_rules = []


##########
# GROUPS #
##########

groups = lgroups.init_groups(group_rules)
groups.append(lgroups.init_scratchpad())


############
# KEYBINDS #
############

mouse = keybinds.init_mouse()
keys = keybinds.init_keybinds(groups)
dgroups_key_binder = None


#############
# AUTOSTART #
#############

autostart.init()


#########
# QTILE #
#########

cursor_warp = False
follow_mouse_focus = True
bring_front_click = "floating_only"
focus_on_window_activation = "focus"

auto_fullscreen = True
# reconfigure_screens = True

wmname = "Qtile"
# wmname = "LG3D"  # Compatibility with java
main = None  # Will deprecated


##########
# Ending #
##########

end = debug.timing()
debug.log(f"Config finished in {end-start} ms")

# vim: filetype=python:nowrap
